﻿using System;
using CoreGraphics;
using NeatLibrary.iOS;
using UIKit;
using Xamarin.Forms;
using Foundation;

[assembly:Dependency(typeof(CaptureScreen))]
namespace NeatLibrary.iOS
{
	public class CaptureScreen : ICaptureScreen
	{
		public CaptureScreen()
		{
		}

        public void CaptureScreenToAlbum()
		{
			CGSize screenSize = UIScreen.MainScreen.ApplicationFrame.Size;
			CGColorSpace colorSpaceRef = CGColorSpace.CreateDeviceRGB();
            using (var ctx = new CGBitmapContext(IntPtr.Zero, (nint)screenSize.Width, (nint)screenSize.Height, 8, 4 * (int)screenSize.Width, colorSpaceRef, CGImageAlphaInfo.PremultipliedLast))
            {
                ctx.TranslateCTM((nfloat)0, (nfloat)(screenSize.Height));
                ctx.ScaleCTM((nfloat)1.0, (nfloat)(-1.0));
                CurrentViewController().View.Layer.RenderInContext(ctx);
                using(var cgImage = ctx.ToImage())
                {
                    UIImage image = new UIImage(cgImage);
                    if (image == null) return;
                    SaveImageToAlbum(image);
                }
            }
			//cgImage.Dispose();
			//ctx.Dispose();

		}
        public void SaveImageToAlbum(object image)
        {
            try
            {
                UIImage uiImage = image as UIImage;
                if (uiImage == null) return;
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    uiImage.SaveToPhotosAlbum((UIImage img, Foundation.NSError error) =>
                    {

                    });

                });
            }
            catch(Exception exc)
            {

            }
        }
		UIViewController CurrentViewController()
		{
			var window = UIApplication.SharedApplication.KeyWindow;
			var vc = window.RootViewController;
			while (vc.PresentedViewController != null)
			{
			    vc = vc.PresentedViewController;
			}
			return vc;
		}
	}
}
