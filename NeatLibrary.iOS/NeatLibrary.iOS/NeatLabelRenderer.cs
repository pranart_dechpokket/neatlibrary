﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;

using System.Diagnostics;
using NeatLibrary;
using NeatLibrary.iOS;
using Xamarin.Forms.Internals;
using THSplit;
using Foundation;
using System.Text;
using System.Collections.Generic;
using System.Linq;

[assembly: ExportRenderer (typeof (NeatLabel), typeof (NeatLabelRenderer))]
namespace NeatLibrary.iOS
{
    
	public class NeatLabelRenderer : LabelRenderer 
	{
        public static Spliter Spliter { get; set; }
		public UIFont Font { get; set; } 
		static public void Initialize() 
		{ 
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Label> e) {
			base.OnElementChanged (e);
			Recreate ();
		}

		private void Recreate()
		{
            if (Spliter == null) Spliter = new Spliter();

			NeatLabel customLabel = Element as NeatLabel;
			try
			{
				Recreate (customLabel.FontSize);
			}catch{
			}
		}
		int WidthOf(string text)
		{
			var nsText = new NSString(text);

			var size = nsText.GetSizeUsingAttributes(new UIStringAttributes()
			{
				Font = Font,
			});

            return (int)size.Width;
		}
		public string ThaiWrap(string inText,int width)
		{
			List<string> words = Spliter.SegmentByDictionary(inText);
			var count = words.Count;
			StringBuilder sentences = new StringBuilder();
			string currentSentence = string.Empty;

			for(int i = 0; i < count;i++)
			{
    			var currentWord = words.ElementAt(i);

    			if(WidthOf(currentSentence+currentWord) > width)
    			{
    				sentences.Append(currentSentence);
					if(currentSentence != " ")
					{
						sentences.Append(' ');
					}
    				currentSentence = currentWord;
    			}
                else
    			{
    				currentSentence += currentWord;
    				
    			}
			}
            sentences.Append(currentSentence);
			return sentences.ToString();

		}
		private void Recreate (double fontSize)
		{
			NeatLabel customLabel = Element as NeatLabel;
			if (customLabel == null ) return;

			UILabel label = Control as UILabel;
            if (label == null) return;
          
			//string typeface = customLabel.CustomTypeFace;
			nfloat size = (nfloat)fontSize;
            if (!string.IsNullOrWhiteSpace(Element?.StyleId))
            {
                try
                {
                    label.Font = this.Font = UIFont.FromName(Element.StyleId, (System.nfloat)fontSize);
                }
                catch (Exception exc)
                {
                    this.Font = label.Font;
                }
            }
            else
            {
                label.Font = this.Font = UIFont.SystemFontOfSize((nfloat)fontSize);
            }

			if (customLabel.Text == null)
				return;
			string text = customLabel.Text;
			if (customLabel.Text.Length > customLabel.LineLimit-1)
			{
				string str = text.Substring (0, customLabel.LineLimit-1) + "ฯ";
				if (str.Substring (0, 1) == "\"") {
					str = str + "\"";
				}
				text = str;
			}
//			if (customLabel.IsUnderline) {
//				var textUnderline = new NSMutableAttributedString (text);
//				textUnderline.AddAttribute (UIStringAttributeKey.UnderlineStyle, NSNumber.FromNInt (1), new NSRange (0, textUnderline.Length));
//				label.AttributedText = textUnderline;
//			} else {
//				var textUnderline = new NSMutableAttributedString (text);
//				label.AttributedText = textUnderline;
//			}

		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
            switch (e.PropertyName)
            {
                case "CustomTypeFace":
                case "FontSize":
                case "LineLimit":
                case "FontFamily":
                case "IsUnderLine":
                    Recreate();
                    this.SetNeedsDisplay();
                    break;
                case "Renderer":
                    base.OnElementPropertyChanged(sender, e);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (Control != null)
                        {
                            Control.Text = Element.Text;//ThaiWrap(Element?.Text ?? "", (int)(Element?.Width ?? 1)) ?? "";
                        }
                    }
                                                  );
                    break;
                case "Text":
                    Control.Text = Element.Text;//ThaiWrap(Element.Text,(int)Element.Width);
                    break;
				default:
					base.OnElementPropertyChanged (sender, e);
					break;
            }
		}

	}
}

