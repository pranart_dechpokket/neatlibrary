﻿
using Xamarin.Forms;
using System;

namespace NeatLibrary
{
	public class MenuPage : NeatPage
	{
		public MenuListView Menu { get; set; }

		public MenuPage ()
		{
			//Icon = "settings.png";
			Title = "menu"; // The Title property must be set.

			BackgroundImage = "sidemenubackground.png";
			Menu = new MenuListView ();

			NeatGrid grid = new NeatGrid
			{ 
			};
			{
				grid.RowStar (3);
				grid.RowStar (1);

				grid.Add (new Image 
				{
					VerticalOptions = LayoutOptions.Fill,
					HorizontalOptions = LayoutOptions.Fill,
					Source = "bg_tab.png",
					InputTransparent = true,
					Aspect = Aspect.Fill,

				}, 0,0,1,2);

				grid.Add (new Image 
				{
					VerticalOptions = LayoutOptions.Fill,
					HorizontalOptions = LayoutOptions.Fill,
					Source = "bg_icon.png",
					Aspect = Aspect.AspectFit,
					InputTransparent = true,
				},0,0,1,2);

                grid.Add (Menu,0,0,1,2);

			Content = grid;
		}
	}

}

}
