﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace NeatLibrary
{
	public class MenuListView : ListView
	{
		public MenuListView ()
		{
			BackgroundColor = Color.Transparent;
			
			VerticalOptions = LayoutOptions.FillAndExpand;



			var cell = new DataTemplate (typeof(MasterMenuViewCell));
			cell.SetBinding (TextCell.TextProperty, "Title");


			IsGroupingEnabled = true;
			GroupDisplayBinding = new Binding ("Name");
			this.GroupHeaderTemplate = new DataTemplate (typeof(GroupViewCell));
			ItemTemplate = cell;
			RowHeight = 42;
			this.SeparatorColor = Color.Transparent;
		}

        public void ThaiMenu()
        {
           // ObservableCollection<Group> groupedItems = new ObservableCollection<Group>();
            /*
            Group group1 = new Group("DSD Smart Skill & Service", null);
            group1.Add(new MainMenuModel()
            {
                Title = "หน้าแรก",
                IconSource = "label_home_icon.png",
                TargetType = typeof(ComplainMainPage),

            });
            group1.Add(new MainMenuModel()
            {
                Title = "แจ้งเรื่องร้องเรียน",
                IconSource = "label_complain_save_icon.png",
                TargetType = typeof(ComplainSavePage01),

            });
            group1.Add(new MainMenuModel()
            {
                Title = "ติดตามเรื่องร้องเรียน",
                IconSource = "label_complain_search_icon.png",
                TargetType = typeof(ComplainSavePage01),
            }); //hidden when not login
            group1.Add(new MainMenuModel()
            {
                Title = "ข่าวสาร บทความ",
                IconSource = "label_news_icon.png",
                TargetType = typeof(ComplainSavePage01),
            });
            group1.Add(new MainMenuModel()
            {
                Title = "คำถาม - คำตอบ",
                IconSource = "label_faq_icon.png",
                TargetType = typeof(ComplainSavePage01),

            });
            group1.Add(new MainMenuModel()
            {
                Title = "ติดต่อเรา",
                IconSource = "label_contact_icon.png",
                TargetType = typeof(ComplainContactPage),

            });
            group1.Add(new MainMenuModel()
            {
                Title = "ข้อมูลผู้ร้องเรียน",
                IconSource = "label_login_icon.png",
                TargetType = typeof(ComplainContactPage),

            }); //login state

            group1.Add(new MainMenuModel()
            {
                Title = "ลงชื่อผู้ใช้งาน",
                IconSource = "label_login_icon.png",
                TargetType = typeof(ComplainContactPage),

            }); //logout state
            
            groupedItems.Add(group1);
            ItemsSource = groupedItems;
            */
        }

    }
	public class MasterMenuViewCell : NeatViewCell
	{
		public MasterMenuViewCell()
		{
			Grid grid = new Grid 
			{
                BackgroundColor = Color.FromRgba(246, 180, 2,0),
                ColumnSpacing = 0,
			};
            {

                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(5, GridUnitType.Star) });

                {


                    Image imageBackground = new Image
                    {
                        Source = "light_medium_tab.png",
                        VerticalOptions = LayoutOptions.Fill,
                        HorizontalOptions = LayoutOptions.Fill,
                        Aspect = Aspect.Fill,
                        Opacity = 0.5,
                    };
                    grid.Children.Add(imageBackground, 0, 0 + 2, 0, 0 + 1);
                    imageBackground.SetBinding(VisualElement.IsVisibleProperty, new Binding("IsVisible"));


                        Image imageIcon = new Image
                        {
                            VerticalOptions = LayoutOptions.Fill,
                            HorizontalOptions = LayoutOptions.Fill,
                            Aspect = Aspect.AspectFit,
                            Margin = new Thickness(Constant.Scale(8),Constant.Scale(4))
                        };
                        {
                  			imageIcon.SetBinding(Image.SourceProperty, new Binding("IconSource"));
                        }
                     
                    grid.Children.Add(imageIcon, 0, 0);
                }
                NeatLabel labelName = new NeatLabel
                {
                    FontSize = Device.GetNamedSize(NamedSize.Default, typeof(Label)),

                    TextColor =  Color.FromRgb (102,51,1),
					VerticalOptions = LayoutOptions.Center,
					HorizontalOptions = LayoutOptions.Start,
                   
				};
				{
					labelName.SetBinding (Label.TextProperty,new Binding("Title"));
				}
				grid.Children.Add(labelName,1,0);
			}
			View = grid;
		}
	}

	public class GroupViewCell : NeatViewCell
	{
		public GroupViewCell() : base ()
		{
			Grid grid = new Grid 
			{ 
				BackgroundColor = Color.FromRgb (246,180,2),
			};
			{
				grid.ColumnDefinitions.Add (new ColumnDefinition { Width = new GridLength(1,GridUnitType.Star)});
				grid.ColumnDefinitions.Add (new ColumnDefinition { Width = new GridLength(20,GridUnitType.Star)});
				Image imageBackground = new Image {
					VerticalOptions = LayoutOptions.Fill,
					HorizontalOptions = LayoutOptions.Fill,
					Aspect = Aspect.Fill,	
				};
				{
					imageBackground.SetBinding (Image.SourceProperty, new Binding ("Image"));
				}
				grid.Children.Add(imageBackground,0,0+2,0,0+1);

				NeatLabel labelName = new NeatLabel 
				{
					FontSize = Device.GetNamedSize (NamedSize.Default, typeof(Label)),
					TextColor = Color.FromRgb (102,51,1),
					VerticalOptions = LayoutOptions.Center,
					HorizontalOptions = LayoutOptions.Start,
				};
				{
					labelName.SetBinding (Label.TextProperty,new Binding("Name"));
				}
				grid.Children.Add(labelName,1,0);
			}
			View = grid;
		}
	}
	public class Group : ObservableCollection<MainMenuModel>
	{
		public string Name { get; private set; }        
		public string Image { get; set; }

		public Group(string Name,string Image)
		{
			this.Name = Name;                
			this.Image = Image;

		}
		// Whatever other properties
	}

	public class MainMenuModel : INotifyPropertyChanged 
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public string Title { get; set; }

		public string IconSource { get; set; }

		public Type TargetType { get; set; }

		private bool _isVisible = true;

		public bool IsVisible
		{  
			get { return _isVisible; } 
			set  
			{  
				_isVisible = value; 

				if ( PropertyChanged != null ) 
				{ 
					PropertyChanged( this, new PropertyChangedEventArgs( "IsVisible" ) );
				} 
			} 
		} 
	}
}


