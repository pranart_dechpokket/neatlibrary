﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Linq;

namespace NeatLibrary
{
    public partial class BasePage : ContentPage
    {
        public BasePage(string TopicImageFilename, string backgroundImageFilename = "background.png")
        {
            InitializeComponent();

            ImageBackground.Source = backgroundImageFilename;
            ImageTitle.Source = TopicImageFilename;

            NavigationPage.SetHasNavigationBar(this, false);

        }
      
        public View Core { get; set; } = new ContentView();
        public void Connect()
        {
            Custom.Content = Core;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            ButtonBack.Clicked += ButtonBack_Clicked;
        }
        protected override void OnDisappearing()
        {
            ButtonBack.Clicked -= ButtonBack_Clicked;
            base.OnDisappearing();
        }

        void ButtonBack_Clicked(object sender, EventArgs e)
        {
            if (Navigation.NavigationStack.Count > 1)
            {
                Navigation.PopAsync();
            }
            else
            {
                MessagingCenter.Send<object, Type>(this, "MasterGo", typeof(HomePage));

            }
        }
    }
}

