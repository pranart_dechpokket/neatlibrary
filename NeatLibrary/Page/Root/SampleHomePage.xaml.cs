﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace NeatLibrary
{
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            ButtonSideMenu.Clicked += ButtonSideMenu_Clicked;

            ButtonBook.Clicked += ButtonBook_Clicked;
            ButtonCertify.Clicked += ButtonCertify_Clicked;
            ButtonCenter.Clicked += ButtonCenter_Clicked;
            ButtonNews.Clicked += ButtonNews_Clicked;
            ButtonOfficer.Clicked += ButtonOfficer_Clicked;
            ButtonVerifier.Clicked += ButtonVerifier_Clicked;
        }

        protected override void OnDisappearing()
        {
            ButtonSideMenu.Clicked -= ButtonSideMenu_Clicked;

            ButtonBook.Clicked -= ButtonBook_Clicked;
            ButtonCertify.Clicked -= ButtonCertify_Clicked;
            ButtonCenter.Clicked -= ButtonCenter_Clicked;
            ButtonNews.Clicked -= ButtonNews_Clicked;
            ButtonOfficer.Clicked -= ButtonOfficer_Clicked;
            ButtonVerifier.Clicked -= ButtonVerifier_Clicked;

            base.OnDisappearing();
        }

        public void ButtonSideMenu_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send<object>(this, "Master");
        }
        static public Action OnBook { get; set; }
        virtual public void ButtonBook_Clicked(object sender, EventArgs e)
        {
        }

        virtual public void ButtonCertify_Clicked(object sender, EventArgs e)
        {
            //Navigation.PushAsync(new CertKnowledge());
            // Navigation.PushAsync(new CertDetectIdentityPage25 ());
        }

        virtual public void ButtonCenter_Clicked(object sender, EventArgs e)
        {
            //Navigation.PushAsync(new CenterSearchMapPage4());
        }

        virtual public void ButtonNews_Clicked(object sender, EventArgs e)
        {
            //Navigation.PushAsync(new NewsListPage24());
        }

        virtual public void ButtonOfficer_Clicked(object sender, EventArgs e)
        {
            //Navigation.PushAsync(new OfficerLoginPage12());
        }

        virtual public void ButtonVerifier_Clicked(object sender, EventArgs e)
        {
            //Navigation.PushAsync(new VerifierListPage15());
        }
    }
}

