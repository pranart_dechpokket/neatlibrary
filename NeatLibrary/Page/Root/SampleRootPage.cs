﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace NeatLibrary
{
	public partial class RootPage : MasterDetailPage
	{
		public MenuPage menuPage;

		public RootPage ()
		{
			NavigationPage.SetHasNavigationBar (this,false);
			menuPage = new MenuPage ();

			menuPage.Menu.ItemTapped += (sender, e) => 
			{
				// don't do anything if we just de-selected the row
				if (e.Item == null) return;
				// do something with e.SelectedItem


				NavigateTo ((e.Item as MainMenuModel).TargetType);

				((ListView)sender).SelectedItem = null; // de-select the row

			};
			Master = menuPage;

			Detail = new NavigationPage (Constant.HomePage);
            Constant.INavigation = Detail.Navigation;

			MessagingCenter.Subscribe<object> (this, "Master", (obj) => 
			{
				this.IsPresented = true;
			});

			MessagingCenter.Subscribe<object, Type> (this, "MasterGo", (obj, type) => 
			{
				try 
				{
					NavigateTo (type);
					//menuPage.Menu.SelectedItem = null;
				} 
				catch 
				{
				}
			});
		}

		Type LatestPage { get; set; } = null;

		void NavigateTo (Type menu)
		{
			try 
			{
				Page displayPage = (Page)Activator.CreateInstance (menu);
                this.Detail = new NavigationPage (displayPage);
                Constant.INavigation = this.Detail.Navigation;

				IsPresented = false;
			} 
			catch (System.Exception exc) 
			{
				Debug.WriteLine (exc.ToString ());
			}


		}

	}
}


