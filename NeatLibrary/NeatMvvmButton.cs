﻿using System;
using Xamarin.Forms;
using System.Diagnostics;


namespace NeatLibrary
{
	public class NeatMvvmButton : NeatButton
	{
		public NeatMvvmButton()
		{
			this.VerticalOptions = LayoutOptions.Fill;
			this.HorizontalOptions = LayoutOptions.Fill;
			this.BackgroundColor = Color.Transparent;
			this.BorderColor = Color.Transparent;
			this.BorderWidth = 0;


            if (Device.RuntimePlatform == Device.iOS)
			{
				BackgroundColor = Color.Transparent;
			}
			else
			{
				Opacity = 0;
			}
			ClickOnce += NeatMvvmButton_ClickOnce;
		}
		public static readonly BindableProperty OnceCommandProperty =
			BindableProperty.Create(nameof(OnceCommandProperty), typeof(Command), typeof(NeatMvvmButton), new Command(() => { }), BindingMode.Default);
		public Command OnceCommand
		{
			get { return (Command)GetValue(OnceCommandProperty); }
			set { SetValue(OnceCommandProperty, value); }
		}

		void NeatMvvmButton_ClickOnce(object sender, EventArgs e)
		{
			if(OnceCommand != null && OnceCommand.CanExecute(this))
			{
				OnceCommand.Execute(this);
			}
		}

	}
}

