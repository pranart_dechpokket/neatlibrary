﻿using System;
namespace NeatLibrary
{
    public interface IVersionOs
    {
        int VersionNumber();
        string VersionString();
    }
}
