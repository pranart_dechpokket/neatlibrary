﻿using System;
namespace NeatLibrary
{
	public interface ICaptureScreen
	{
        void CaptureScreenToAlbum();
        void SaveImageToAlbum(object oImage);
	}
}
