﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class SafeView : View
    {
        public SafeView()
        {
            BackgroundColor = Color.Transparent;
        }
        public object NativeView { get; set; }
    }
}
