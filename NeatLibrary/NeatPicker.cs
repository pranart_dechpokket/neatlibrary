﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class NeatPicker : FormsTest.Picker
	{
		public NeatPicker()
		{
			VerticalOptions = LayoutOptions.Fill;
			HorizontalOptions = LayoutOptions.Fill;
			this.SelectedIndexChanged += Handle_SelectedIndexChanged;

            if (Device.RuntimePlatform == Device.Android)
			{
				Opacity = 0;
			}
		}

		public static readonly BindableProperty SelectedIndexChangedCommandProperty =
			BindableProperty.Create(nameof(SelectedIndexChangedCommandProperty), typeof(Command), typeof(NeatPicker), new Command(() => { }), BindingMode.Default);
		public Command SelectedIndexChangedCommand
		{
			get { return (Command)GetValue(SelectedIndexChangedCommandProperty); }
			set { SetValue(SelectedIndexChangedCommandProperty, value); }
		}

		void Handle_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (SelectedIndexChangedCommand.CanExecute(this.SelectedItem))
			{
				SelectedIndexChangedCommand.Execute(this.SelectedItem);
			}
		}
	}
}

