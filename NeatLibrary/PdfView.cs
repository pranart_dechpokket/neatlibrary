﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
	public class PdfView : View
	{
		public PdfView ()
		{
		}

		public static readonly BindableProperty PathProperty =
			BindableProperty.Create("Path",typeof(string),typeof(PdfView),"");

		public string Path
		{
			get {return (string)base.GetValue (PathProperty);}
			set {base.SetValue (PathProperty, value);}
		}

	}
}

