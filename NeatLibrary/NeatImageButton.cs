﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class NeatImageButton : NeatGrid
    {
        public NeatImage Image { get;  protected set; }
        public NeatButton Button { get; protected set; }

        public NeatImageButton()
        {
            Image = new NeatImage();
            this.Add(Image);
            Button = new NeatButton();
            this.Add(Button);

            Button.Clicked += (sender, e) => 
            {
                Clicked?.Invoke(sender, e);
                if(Command?.CanExecute(sender) ?? false)
                {
                    Command?.Execute(sender);
                }
            };
        }

        public event EventHandler<EventArgs> Clicked;


        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(nameof(CommandProperty), typeof(Command), typeof(NeatImageButton), new Command(() => { }), BindingMode.Default);
        public Command Command
        {
            get { return (Command)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public ImageSource Source
        {
            get 
            {
                return Image?.Source;
            }
            set 
            {
                Image.Source = value;
            }
        }

        public Aspect Aspect
        {
            get 
            {
                return Image.Aspect;
            }
            set 
            {
                Image.Aspect = value;
            }
        }


    }
}
