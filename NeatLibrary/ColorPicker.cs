﻿using System;
using System.Linq;
using NControl.Abstractions;
using NGraphics;
using Xamarin.Forms;
namespace NeatLibrary
{
    public class ColorPicker : NControlView
    {
        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
        }
        public double Step { get; set; } = 10;
        public override void Draw(ICanvas canvas, Rect rect)
        {
            base.Draw(canvas, rect);

            double unitWidth = rect.Width / Step;
            double unitHeight = rect.Height / Step;

            double unitLuminosity = 1.0 / Step;
            double unitHue = 1.0 / Step;

            double y = 0;


            double hue = 0.0;
            for (int iHue = 0; iHue < Step; iHue++)
            {
                double x = 0;
                double luminosity = 0;
                for (int iLuminosity = 0; iLuminosity < Step; iLuminosity++  )
                {
                    var painter = new SolidBrush(Xamarin.Forms.Color.FromHsla(hue, 1.0, luminosity).ToCross());
                    var rectangle = new NGraphics.Rect((int)x, (int)y, (int)(unitWidth), (int)(unitHeight));
                    canvas.DrawRectangle(rectangle, new NGraphics.Size(0, 0), new Pen(Xamarin.Forms.Color.Transparent.ToCross()), painter);

                    x += unitWidth;
                    luminosity += unitLuminosity;
                }
                y += unitHeight;
                hue += unitHue;
            }
        }
        public event EventHandler<Xamarin.Forms.Color> OnSelectColor;
        public override bool TouchesBegan(System.Collections.Generic.IEnumerable<NGraphics.Point> points)
        {
            var point = points.First();

            float luminosity = (float)Math.Round(point.X / this.Width-0.05,1);
            float hue = (float)Math.Round(point.Y / this.Height-0.05, 1);

            OnSelectColor?.Invoke(this, Xamarin.Forms.Color.FromHsla(hue, 1.0, luminosity));
            return base.TouchesBegan(points);
        }
    }
}
