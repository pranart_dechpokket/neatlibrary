﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
	public class NeatViewCell : ViewCell
	{
		public static readonly BindableProperty SelectedBackgroundColorProperty =
        BindableProperty.Create("SelectedBackgroundColor",
                                typeof(Color),
                                typeof(NeatViewCell),
			                    Xamarin.Forms.Color.Accent);

        public Color SelectedBackgroundColor
        {
            get { return (Color)GetValue(SelectedBackgroundColorProperty); }
            set { SetValue(SelectedBackgroundColorProperty, value); }
        }
	}
}

 