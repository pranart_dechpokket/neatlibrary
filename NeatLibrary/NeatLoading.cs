﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class NeatLoading : FormsTest.ActivityIndicator
	{
		public NeatLoading ()
		{
			IsRunning = true;
			VerticalOptions = LayoutOptions.Center;
			HorizontalOptions = LayoutOptions.Center;

		}
	}
}

