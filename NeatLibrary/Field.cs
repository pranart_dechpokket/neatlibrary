﻿using System;
using Realms;
using PropertyChanged;

namespace NeatLibrary
{
    [ImplementPropertyChanged]
    public class Field 
    {
        public string Name { get; set; } = string.Empty;
        public bool IsNotValid { get; set; }
        public string NotValidMessageError { get; set; } = string.Empty;
    }
}
