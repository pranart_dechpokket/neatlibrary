﻿using System;
using Xamarin.Forms;
using NControl.Controls;
using System.ComponentModel;

namespace NeatLibrary
{
	public class NeatSvgImage : SvgImage
	{

		public NeatSvgImage()
		{
			this.OnTouchesBegan += Handle_OnTouchesBegan;
			this.OnTouchesEnded += Handle_OnTouchesEnded;
			this.OnTouchesCancelled += Handle_OnTouchesCancelled;
			this.OnTouchesMoved += Handle_OnTouchesMoved;

		}

		public static readonly BindableProperty OnTouchesBeganCommandProperty =
			BindableProperty.Create(nameof(OnTouchesBeganCommandProperty), typeof(Command), typeof(NeatSvgImage), new Command(() => { }), BindingMode.Default);
		public Command OnTouchesBeganCommand
		{
			get { return (Command)GetValue(OnTouchesBeganCommandProperty); }
			set { SetValue(OnTouchesBeganCommandProperty, value); }
		}

		void Handle_OnTouchesBegan(object sender, System.Collections.Generic.IEnumerable<NGraphics.Point> e)
		{
			if (OnTouchesBeganCommand.CanExecute(e))
			{
				OnTouchesBeganCommand.Execute(e);
			}
		}

		public static readonly BindableProperty OnTouchesEndedCommandProperty =
			BindableProperty.Create(nameof(OnTouchesEndedCommandProperty), typeof(Command), typeof(NeatSvgImage), new Command(() => { }), BindingMode.Default);
		public Command OnTouchesEndedCommand
		{
			get { return (Command)GetValue(OnTouchesEndedCommandProperty); }
			set { SetValue(OnTouchesEndedCommandProperty, value); }
		}

		void Handle_OnTouchesEnded(object sender, System.Collections.Generic.IEnumerable<NGraphics.Point> e)
		{
			if (OnTouchesEndedCommand.CanExecute(e))
			{
				OnTouchesEndedCommand.Execute(e);
			}
		}

		public static readonly BindableProperty OnTouchesCancelledCommandProperty =
			BindableProperty.Create(nameof(OnTouchesCancelledCommandProperty), typeof(Command), typeof(NeatSvgImage), new Command(() => { }), BindingMode.Default);
		public Command OnTouchesCancelledCommand
		{
			get { return (Command)GetValue(OnTouchesCancelledCommandProperty); }
			set { SetValue(OnTouchesCancelledCommandProperty, value); }
		}

		void Handle_OnTouchesCancelled(object sender, System.Collections.Generic.IEnumerable<NGraphics.Point> e)
		{
			if (OnTouchesCancelledCommand.CanExecute(e))
			{
				OnTouchesCancelledCommand.Execute(e);
			}
		}

		public static readonly BindableProperty OnTouchesMovedCommandProperty =
			BindableProperty.Create(nameof(OnTouchesMovedCommandProperty), typeof(Command), typeof(NeatSvgImage), new Command(() => { }), BindingMode.Default);
		public Command OnTouchesMovedCommand
		{
			get { return (Command)GetValue(OnTouchesMovedCommandProperty); }
			set { SetValue(OnTouchesMovedCommandProperty, value); }
		}

		void Handle_OnTouchesMoved(object sender, System.Collections.Generic.IEnumerable<NGraphics.Point> e)
		{
			if (OnTouchesMovedCommand.CanExecute(e))
			{
				OnTouchesMovedCommand.Execute(e);
			}
		}
	}
}
