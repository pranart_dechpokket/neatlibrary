﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class OnceButton : FormsTest.Button
    {
        public OnceButton()
        {
            this.VerticalOptions = LayoutOptions.Fill;
            this.HorizontalOptions = LayoutOptions.Fill;

            Clicked += NeatButton_Clicked;

        }
        public object Tag { get; set; }
        public event EventHandler ClickOnce;
        public static DateTime LastClickTime { get; set; } = DateTime.MinValue;

        void NeatButton_Clicked(object sender, System.EventArgs e)
        {
            if ((DateTime.Now - LastClickTime).Milliseconds > 100)
            {
                ClickOnce?.Invoke(this, null);
                LastClickTime = DateTime.Now;
            }
        }

    }
}
