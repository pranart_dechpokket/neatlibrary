﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class NeatFrame : Frame
	{
		public NeatFrame ()
		{
			this.BorderColor = Color.Transparent;
			this.HasShadow = false;

			this.HorizontalOptions = LayoutOptions.Fill;
			this.VerticalOptions = LayoutOptions.Fill;
			this.BackgroundColor = Color.Transparent;
			Padding = new Thickness(5);
		}
	}
}

