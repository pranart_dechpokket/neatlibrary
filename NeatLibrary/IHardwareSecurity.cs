﻿namespace NeatLibrary
{
    public interface IHardwareSecurity
    {
        bool IsJailBreaked();
        bool IsInEmulator();
    }
}