﻿using System;
using System.Diagnostics;
using System.Linq;
using NControl.Abstractions;

namespace NeatLibrary
{
    public class FrameSizer : NControlView
    {
        public Pointer StartPoint { get; set; } = new Pointer { XPercent = 0.33, YPercent = 0.33 };
        public Pointer StopPoint { get; set; } = new Pointer { XPercent = 0.66, YPercent = 0.66 };
        public NGraphics.Rect LastRect { get; set; }
        public double RectWidth => StopPoint.X(LastRect) - StartPoint.X(LastRect);
        public double RectHeight => StopPoint.Y(LastRect) - StartPoint.Y(LastRect);
        public NGraphics.Rect DrawRect => new NGraphics.Rect(StartPoint.X(LastRect), StartPoint.Y(LastRect),RectWidth, RectHeight);
        public SelectPointer Selected { get; set; }

        public event EventHandler Changed;
        public FrameSizer()
        {
        }
        public void To(out double x, out double y,out double width,out double height)
        {
            x = StartPoint.XPercent;
            y = (1 - StartPoint.YPercent);
            width = StopPoint.XPercent - StartPoint.XPercent;
            height =  StartPoint.YPercent - StopPoint.YPercent;
        }
        public void From(double x, double y, double width, double height)
        {
            StartPoint = new Pointer { XPercent = x, YPercent = (1 - y), };
            StopPoint = new Pointer { XPercent = x + width, YPercent = (1 - y) - height, };
        }
        public override void Draw(NGraphics.ICanvas canvas, NGraphics.Rect rect)
        {
            //base.Draw(canvas, rect);
            LastRect = rect;

            canvas.DrawRectangle(DrawRect, new NGraphics.Size(), new NGraphics.Pen(NGraphics.Colors.Black));
            DrawPointer(canvas,StartPoint.X(LastRect),StartPoint.Y(LastRect));
            DrawPointer(canvas,StopPoint.X(LastRect),StopPoint.Y(LastRect));
        }
        public void DrawPointer(NGraphics.ICanvas canvas,double x,double y)
        {
            canvas.DrawEllipse(new NGraphics.Rect(x-4,y-4,8,8),new NGraphics.Pen(NGraphics.Colors.Black));
        }
        Pointer BeforeMoveStart { get; set; }
        Pointer BeforeMoveStop { get; set; }
        NGraphics.Point BeforeMovePoint { get; set; }

        public override bool TouchesBegan(System.Collections.Generic.IEnumerable<NGraphics.Point> points)
        {
            var point = BeforeMovePoint = points.First();
            if(StartPoint.X(LastRect)-4 <= point.X && point.X <= StartPoint.X(LastRect)+4 &&
               StartPoint.Y(LastRect)-4 <= point.Y && point.Y <= StartPoint.Y(LastRect)+4)
            {
                Selected = SelectPointer.StartPoint;
                Debug.WriteLine("StartPoint");
            }
            else if
                (
                    StopPoint.X(LastRect) - 4 <= point.X && point.X <= StopPoint.X(LastRect) + 4 &&
                    StopPoint.Y(LastRect) - 4 <= point.Y && point.Y <= StopPoint.Y(LastRect) + 4
                )
            {
                Selected = SelectPointer.StopPoint;
            }
            else
            {
                BeforeMoveStart = StartPoint.Clone();
                BeforeMoveStop = StopPoint.Clone();

                Selected = SelectPointer.All;
            }

            return base.TouchesBegan(points);
        }
        public override bool TouchesMoved(System.Collections.Generic.IEnumerable<NGraphics.Point> points)
        {
            Debug.WriteLine("Moving");
            switch(Selected)
            {
                case SelectPointer.StartPoint:
                    StartPoint.SetPosition(LastRect, points.First().X, points.First().Y);
                    Changed?.Invoke(this, new EventArgs());
                    Invalidate();
                    break;
                case SelectPointer.StopPoint:
                    StopPoint.SetPosition(LastRect, points.First().X, points.First().Y);
                    Changed?.Invoke(this, new EventArgs());
                    Invalidate();
                    break;
                case SelectPointer.All:
                    double dx = points.First().X - BeforeMovePoint.X;
                    double dy = points.First().Y - BeforeMovePoint.Y;
                    StartPoint = BeforeMoveStart.Dif(LastRect,dx, dy);
                    StopPoint = BeforeMoveStop.Dif(LastRect,dx, dy);
                    Changed?.Invoke(this, new EventArgs());
                    Invalidate();
                    break;
                case SelectPointer.None:
                default:
                    break;
            }


            return base.TouchesMoved(points);

        }
        public override bool TouchesEnded(System.Collections.Generic.IEnumerable<NGraphics.Point> points)
        {
            Debug.WriteLine("End");
            Selected = SelectPointer.None;

            return base.TouchesEnded(points);
        }
        public override bool TouchesCancelled(System.Collections.Generic.IEnumerable<NGraphics.Point> points)
        {
            return base.TouchesCancelled(points);
        }
    }
    public enum SelectPointer
    {
        None,
        StartPoint,
        StopPoint,
        All
    }
    public class Pointer
    {
        public double XPercent { get; set; }
        public double YPercent { get; set; }

        public Pointer Dif(NGraphics.Rect rect,double dx, double dy)
        {
            var newPoint = new Pointer();
            newPoint.SetPosition(rect,X(rect) + dx, Y(rect) + dy);
            return newPoint;
        }
        public double X(NGraphics.Rect rect)
        {
            return XPercent * rect.Width;
        }
        public double Y(NGraphics.Rect rect)
        {
            return YPercent * rect.Height;
        }
        public double UY(NGraphics.Rect rect)
        {
            return (1.0 - YPercent) * rect.Height;
        }
        public void SetPosition(NGraphics.Rect rect,double x,double y)
        {
            XPercent = x / rect.Width;
            YPercent = y / rect.Height;
        }
        public Pointer Clone()
        {
            return new Pointer
            {
                XPercent = this.XPercent,
                YPercent = this.YPercent,
            };
        }
    }
}
