using Xamarin.Forms;

namespace NeatLibrary
{
	public class BottomTabbedPage : TabbedPage
	{
		public bool FixedMode { get; set; }

		public void RaiseCurrentPageChanged()
		{
			OnCurrentPageChanged();
		}
	}
}