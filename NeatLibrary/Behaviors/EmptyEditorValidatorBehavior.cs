﻿using System;
namespace NeatLibrary
{
    using System;
    using Xamarin.Forms;

    namespace NeatLibrary
    {
        public class EmptyEditorValidatorBehavior : Behavior<NeatEditor>
        {
            NeatEditor control;

            protected override void OnAttachedTo(NeatEditor bindable)
            {
                bindable.TextChanged += HandleTextChanged;
                bindable.PropertyChanged += OnPropertyChanged;
                control = bindable;
                //_placeHolder = bindable.Placeholder;
                //_placeHolderColor = bindable.PlaceholderColor;
            }

            void HandleTextChanged(object sender, TextChangedEventArgs e)
            {
                if (!string.IsNullOrEmpty(e.NewTextValue))
                {
                    ((NeatEditor)sender).IsBorderErrorVisible = false;
                }
            }

            protected override void OnDetachingFrom(NeatEditor bindable)
            {
                bindable.TextChanged -= HandleTextChanged;
                bindable.PropertyChanged -= OnPropertyChanged;
            }

            void OnPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
            {
                if (e.PropertyName == NeatEditor.IsBorderErrorVisibleProperty.PropertyName && control != null)
                {
                    if (control.IsBorderErrorVisible)
                    {
                        //control.Placeholder = control.ErrorText;
                        //control.PlaceholderColor = control.BorderErrorColor;
                        control.Text = string.Empty;
                    }

                    else
                    {
                        //control.Placeholder = _placeHolder;
                        //control.PlaceholderColor = _placeHolderColor;
                    }

                }
            }
        }
    }
}
