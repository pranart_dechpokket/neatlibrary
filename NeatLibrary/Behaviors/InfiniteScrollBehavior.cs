﻿using System;
using System.Collections;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class InfiniteScrollBehavior : Behavior<NeatListView>
    {
        public static readonly BindableProperty LoadMoreCommandProperty 
        = BindableProperty.Create
                          ("LoadMoreCommand", typeof(ICommand), typeof(InfiniteScrollBehavior), null);
        public ICommand LoadMoreCommand
        {
            get
            {
                return (ICommand)GetValue(LoadMoreCommandProperty);
            }
            set
            {
                SetValue(LoadMoreCommandProperty, value);
            }
        }
        public NeatListView AssociatedObject
        {
            get;
            private set;
        }
        protected override void OnAttachedTo(NeatListView bindable)
        {
            base.OnAttachedTo(bindable);
            AssociatedObject = bindable;
            bindable.BindingContextChanged += Bindable_BindingContextChanged;
            bindable.ItemAppearing += InfiniteNeatListView_ItemAppearing;
        }
        private void Bindable_BindingContextChanged(object sender, EventArgs e)
        {
            OnBindingContextChanged();
        }
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            BindingContext = AssociatedObject.BindingContext;
        }
        protected override void OnDetachingFrom(NeatListView bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.BindingContextChanged -= Bindable_BindingContextChanged;
            bindable.ItemAppearing -= InfiniteNeatListView_ItemAppearing;
        }
        void InfiniteNeatListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var items = AssociatedObject.ItemsSource as IList;
            if (items != null && e.Item == items[items.Count - 1])
            {
                if (LoadMoreCommand != null && LoadMoreCommand.CanExecute(null)) LoadMoreCommand.Execute(null);
            }
        }
    }
}