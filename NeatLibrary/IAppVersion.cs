﻿using System;
namespace NeatLibrary
{
    public interface IAppVersion
    {
        string Version();
    }
}
