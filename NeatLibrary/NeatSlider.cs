﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class NeatSlider : FormsTest.Slider
	{
		public NeatSlider()
		{
			this.ValueChanged += (sender, e) => 
			{
				if(CommandSlider == null)
				{
					return;
				}
				if(CommandSlider.CanExecute(this))
				{
					CommandSlider.Execute(this);
				}
			};
		}
		public static readonly BindableProperty CommandSliderProperty =
			BindableProperty.Create(nameof(CommandSliderProperty), typeof(Command), typeof(NeatSlider), null, BindingMode.OneWay);

		public Command CommandSlider
		{
			get { return (Command)GetValue(CommandSliderProperty);}
			set { SetValue(CommandSliderProperty, value);}
		}
	}
}
