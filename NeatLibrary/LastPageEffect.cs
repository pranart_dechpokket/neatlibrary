﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class LastPageEffect : RoutingEffect
    {
        public LastPageEffect() : base("NeatLibrary.LastPageEffect")
        {
        }
    }
}
