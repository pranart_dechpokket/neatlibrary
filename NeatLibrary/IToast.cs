﻿using System;
namespace NeatLibrary
{
    public interface IToast
    {
        void Toast(string message);
    }
}
