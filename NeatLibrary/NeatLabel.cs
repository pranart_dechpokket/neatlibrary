﻿using System;
using Xamarin.Forms;

namespace NeatLibrary
{
    public class NeatLabel : FormsTest.Label
	{
		public NeatLabel ()
		{
            //FontFamily = "THSarabunNew";
            //FontSize = Device.GetNamedSize(NamedSize.Large, this);
            FontSize = Device.GetNamedSize(NamedSize.Medium, this);
        }


        public static readonly BindableProperty IsBoldProperty =
            //BindableProperty.Create<NeatLabel,bool>(X=>X.IsBold,false);
            BindableProperty.Create(nameof(IsBold), typeof(bool), typeof(NeatLabel),false);

		public bool IsBold
		{
            get { return (bool)base.GetValue (IsBoldProperty); }
            set { base.SetValue (IsBoldProperty, value); }

		}

        public static readonly BindableProperty LineLimitProperty =
            //BindableProperty.Create<NeatLabel,int>(X=>X.LineLimit,10000);
            BindableProperty.Create(nameof(LineLimit), typeof(int), typeof(NeatLabel),10000);

		public int LineLimit
		{
			get { return (int)base.GetValue (LineLimitProperty); }
			set { base.SetValue (LineLimitProperty, value); }

		}
	}
}

