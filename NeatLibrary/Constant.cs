﻿using System;
using Xamarin.Forms;
using System.Linq;
using System.Text.RegularExpressions;

namespace NeatLibrary
{
	public class Constant
	{
		static public Func<bool> IsInitialized { get; set; }
		static public int Scale(int x)
		{
			if (Device.Idiom == TargetIdiom.Tablet)
			{
				return 2 * x;
			}
			return x;
		}
		static public double Scale(double x)
		{
			if (Device.Idiom == TargetIdiom.Tablet)
			{
				return 2 * x;
			}
			return x;
		}
		static public ContentPage HomePage { get; set; }
		static public int RepeatWebServiceCount = int.MaxValue;
		static public INavigation INavigation { get; set; }

		static public bool CheckID13(string ID)
		{
			Regex regex = new Regex("[0-9]{13}");
			return regex.IsMatch(ID);
		}
		static public bool CheckWebSite(string url)
		{
			url = url.Trim();
			Regex regex = new Regex("^(http\\:\\/\\/|https\\:\\/\\/)?([a-z0-9A-Z][a-z0-9A-Z\\-]*\\.)+[a-z0-9A-Z][a-z0-9A-Z\\-]*$");
			return regex.IsMatch(url);
		}
		static public bool CheckEmail(string email)
		{
			email = email.Trim();
			Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
			return regex.IsMatch(email);
		}
		static public bool CheckZipCode(string ZipCode)
		{
			ZipCode = ZipCode.Trim();
			Regex regex = new Regex("[0-9]{5}");
			return regex.IsMatch(ZipCode);
		}
		static public double FontSpecialMicro;
        static public double FontMicro;
        static public double FontSmaller;
        static public double FontSmallest;

		static public double FontSmall;//= Neat.Font((Device.OS == TargetPlatform.iOS) ? Device.GetNamedSize(NamedSize.Micro, typeof(Label)) : Device.GetNamedSize(NamedSize.Small, typeof(Label)));
		static public double FontDefault;// = Neat.Font((Device.OS == TargetPlatform.iOS) ? Device.GetNamedSize(NamedSize.Small, typeof(Label)) : Device.GetNamedSize(NamedSize.Default, typeof(Label)));
		static public double FontMedium;// = Neat.Font((Device.OS == TargetPlatform.iOS) ? Device.GetNamedSize(NamedSize.Default, typeof(Label)) : Device.GetNamedSize(NamedSize.Medium, typeof(Label)));
		static public double FontLarge;// = Neat.Font((Device.OS == TargetPlatform.iOS) ? Device.GetNamedSize(NamedSize.Medium, typeof(Label)) : Device.GetNamedSize(NamedSize.Large, typeof(Label)));
		static public double FontLarger;// = Neat.Font((Device.OS == TargetPlatform.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.1);
		static public double FontBig;// = Neat.Font((FontLarge + FontMedium) / 2);
		static public double FontLargest;// = Neat.Font((Device.OS == TargetPlatform.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.1 : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.2);
		static public double FontHuge;// = Neat.Font((Device.OS == TargetPlatform.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.2 : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.3);
		static public double FontHuger;// = Neat.Font((Device.OS == TargetPlatform.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.3 : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.4);
		static public double FontHugest;// = Neat.Font((Device.OS == TargetPlatform.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.4 : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.5);

		static public void Initialize()
		{
            FontSpecialMicro = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Micro, typeof(Label)) / 2.0 : Device.GetNamedSize(NamedSize.Micro, typeof(Label)) / 2.1);
            FontMicro = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Micro, typeof(Label))/1.8 : Device.GetNamedSize(NamedSize.Micro, typeof(Label)) / 1.4);
            FontSmallest = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Micro, typeof(Label)) / 1.7 : Device.GetNamedSize(NamedSize.Small, typeof(Label)) / 1.5);
            FontSmaller = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Micro, typeof(Label)) / 1.4 : Device.GetNamedSize(NamedSize.Small, typeof(Label)) / 1.3);

            FontSmall = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Micro, typeof(Label)) : Device.GetNamedSize(NamedSize.Small, typeof(Label)));
            FontDefault = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Small, typeof(Label)) : Device.GetNamedSize(NamedSize.Default, typeof(Label)));
            FontMedium = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Default, typeof(Label)) : Device.GetNamedSize(NamedSize.Medium, typeof(Label)));
            FontLarge = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Medium, typeof(Label)) : Device.GetNamedSize(NamedSize.Large, typeof(Label)));
            FontLarger = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.1);
			FontBig = Neat.Font((FontLarge + FontMedium) / 2);
            FontLargest = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.1 : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.2);
            FontHuge = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.2 : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.3);
            FontHuger = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.3 : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.4);
            FontHugest = Neat.Font((Device.RuntimePlatform == Device.iOS) ? Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.4 : Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 1.5);
		}
	}
}

