# Getting Started with AnimationNavigationPage
AnimationNavigationPage component for Xamarin.Forms includes four libraries: PCL, Touch and Droid.

AnimationNavigationPage is a NavigationPage with custom page transitions animation effects.

This document provides a short guide to getting started.

## Creating a new project

Create Blank App (Xamarin.Forms.Portable). Three projects will be created automaticaly:
 - YourProject
 - YourProject.Droid
 - YourProject.iOS

Add references to projects:
 - YourProject : FormsControls.dll
 - YourProject.Droid : FormsControls.dll , FormsControls.Droid.dll
 - YourProject.iOS : FormsControls.dll , FormsControls.Touch.dll

 Call FormsControls.Touch.Main.Init(), in AppDelegate.cs (FinishedLaunching method) and FormsControls.Droid.Main.Init() in MainActivity.cs (OnCreate method) 
 ```csharp   
public override bool FinishedLaunching(UIApplication app, NSDictionary options)
    	{
        	Xamarin.Forms.Forms.Init();
        	FormsControls.Touch.Main.Init();
        	LoadApplication (new App ());
        	return base.FinishedLaunching(app, options);
    	}
```
 ```csharp   
  protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Xamarin.Forms.Forms.Init(this, bundle);
            FormsControls.Droid.Main.Init(this);
            LoadApplication(new App());
        }
```

We already provided Sample solution to show the easiest way to use AnimationNavigationPage component

## Main requirements

 - Replase NavigationPage to AnimationNavigationPage.

 - Create AnimationPage instead ContentPages or Implement IPageAnimation interfece in your Pages.

 - To change transition effects  - PageAnimation property can be setted to: EmptyAnimation, DefaultAnimation, FlipAnimation, FadeAnimation, SlideAnimation, LandingAnimation, RollAnimation, RotateAnimation.
- Also you cant change animation subtype (PageAnimation.Subtype): Default, FromLeft, FromRight, FromTop, FromBottom.
