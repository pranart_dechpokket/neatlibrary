﻿using FormsControls.Base;
using Xamarin.Forms;

namespace Sample
{
    public partial class EmptyAnimationsPage : ContentPage, IAnimationPage
    {
        private IPageAnimation _pageAnimation;

        public EmptyAnimationsPage()
        {
            InitializeComponent();
        }

        public IPageAnimation PageAnimation =>  _pageAnimation ?? (_pageAnimation = new EmptyPageAnimation());
    }
}