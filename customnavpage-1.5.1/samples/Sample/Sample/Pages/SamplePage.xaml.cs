﻿using System;
using FormsControls.Base;

namespace Sample
{
    public partial class SamplePage : AnimationPage
    {
        public SamplePage ()
        {
            InitializeComponent ();
        }

        private void OnNextButtonClicked (object sender, EventArgs args)
        {
            Navigation.PushAsync (new SamplePage { Title = PageAnimation.Subtype.ToString (), PageAnimation = PageAnimation });
        }

        private void OnBackButtonClicked (object sender, EventArgs args)
        {
            Navigation.PopAsync ();
        }

        private void OnPopToPootButtonClicked (object sender, EventArgs args)
        {
            Navigation.PopToRootAsync ();
        }
    }
}