﻿using System;
using Xamarin.Forms.Platform.Android;
using NeatLibrary.Droid;
using Xamarin.Forms;
using NeatLibrary;
using Android.Views;

[assembly:ExportRenderer(typeof(NeatPicker),typeof(NeatPickerRenderer))]
namespace NeatLibrary.Droid
{
	public class NeatPickerRenderer : PickerRenderer
	{
		public NeatPickerRenderer ()
		{
		}
		public static void Initialize() { }
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.Picker> e)
		{
			base.OnElementChanged (e);
			Control.Gravity = GravityFlags.CenterHorizontal;
		}
	}
}

