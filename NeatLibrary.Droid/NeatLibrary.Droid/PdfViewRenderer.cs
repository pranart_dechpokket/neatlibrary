﻿using System;
using Com.Joanzapata.Pdfview;
using Com.Joanzapata.Pdfview.Listener;
using NeatLibrary;
using NeatLibrary.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly:ExportRenderer(typeof(PdfView),typeof(PdfViewRenderer))]
namespace NeatLibrary.Droid
{
    public class PdfViewRenderer : ViewRenderer<PdfView, APdfView>, IOnPageChangeListener
    {
        APdfView PDFView { get; set; }

        public PdfViewRenderer()
        {
        }

		public static void Initialize() { }

        protected override void OnElementChanged(ElementChangedEventArgs<PdfView> e)
        {
            base.OnElementChanged(e);

            PDFView = new APdfView(Forms.Context, null);
            SetNativeControl(PDFView);


        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "Path" && Element != null)
            {
                Device.BeginInvokeOnMainThread(() =>
               {
                   try
                   {
                       PDFView.FromFile(new Java.IO.File(Element.Path))
                           .DefaultPage(1)
                           .OnPageChange(this)
                           .EnableDoubletap(true)
                           .Load();
                   }
                   catch (Exception)
                   {
                   }
               });
            }
        }

        void IOnPageChangeListener.OnPageChanged(int p0, int p1)
        {

        }
    }
}