﻿using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using NeatLibrary;

[assembly:ExportRenderer(typeof(NeatWebView),typeof(NeatLibrary.Droid.NeatWebViewRenderer))]
namespace NeatLibrary.Droid
{
	public class NeatWebViewRenderer : WebViewRenderer
	{
		public NeatWebViewRenderer ()
		{
		}
		public static void Initialize() { }
		protected override void OnElementChanged (ElementChangedEventArgs<WebView> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{

				Control.Settings.BuiltInZoomControls = true;
				Control.Settings.DisplayZoomControls = true;
			}

		}
	}
}

