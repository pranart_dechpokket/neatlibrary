﻿using System;
namespace NeatLibrary.Droid
{
	public static class Neat
	{
		static public void Initialize()
		{
			//PdfViewRenderer.Initialize();
			NeatEntryRenderer.Initialize();
			NeatPickerRenderer.Initialize();
			NeatWebViewRenderer.Initialize();
			HybridWebViewRenderer.Initialize();
			NeatNavigationRenderer.Initialize();
			FacebookLoginPageRenderer.Initialize();
			InstagramLoginPageRenderer.Initialize();
			NeatDate.Initialize();
            NeatEditorRenderer.Initialize();
			NeatViewCellRenderer.Initialize();
            //BottomBarPageRenderer.Initialize();
		}
	}
}
