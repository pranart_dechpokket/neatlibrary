using Xamarin.Forms;
using NeatLibrary;
using NeatLibrary.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using Android.Util;
using Android.Graphics;
using System;
using NeatLibrary.Shared;
using Android.Content;

[assembly: ExportRenderer(typeof(NeatLabel), typeof(NeatLabelRenderer))]
namespace NeatLibrary.Droid
{
    public class NeatLabelRenderer : LabelRenderer 
	{
        public Context FormsContext { get; set; }

        public NeatLabelRenderer(Context context) : base(context)
        {
            FormsContext = context;
        }
		protected override void OnElementChanged (ElementChangedEventArgs<Label> e) {
			base.OnElementChanged(e);
			var control = new ThaiLineBreakingTextView(FormsContext);
            
			SetNativeControl(control);
			Recreate ();
		}
       
		private void Recreate()
		{
			NeatLabel customLabel = Element as NeatLabel;
			Recreate (customLabel.FontSize);
		}
		private void Recreate (double fontSize)
		{
			TextView label = (TextView)Control; 
			try
			{
                if(!string.IsNullOrEmpty(Element?.StyleId))
                {
                    Typeface font = Typeface.CreateFromAsset(FormsContext.Assets, Element?.StyleId+".ttf");
                    label.Typeface = font;
                }

             }
            catch(System.Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.ToString());
			}
			label.SetTextSize (ComplexUnitType.Dip,(float)fontSize);
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if(e.PropertyName == "Text" || e.PropertyName=="Renderer")
			{
				(Control as ThaiLineBreakingTextView).SetText2 (Element.Text);			   
			}
			base.OnElementPropertyChanged (sender, e);

			switch(e.PropertyName)
			{
				case "FontSize":
				case "LineLimit":
				case "IsBold":
    				Recreate ();
                    (Control as ThaiLineBreakingTextView).SetText2(Element.Text);
    				this.Invalidate ();
					break;
			}
		}
	}
}
