﻿using System;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Android.Util;
using NeatLibrary;
using NeatLibrary.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(NeatEditor),typeof(NeatEditorRenderer))]
namespace NeatLibrary.Droid
{
    public class NeatEditorRenderer : EditorRenderer
    {
        public NeatEditorRenderer()
        {
        }
        static public void Initialize() {}
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);


            Recreate();

        }
        void Recreate()
        {
            if (this.Control == null) return;
            Control.SetPadding(0,0,0,0);

            Control.SetHintTextColor(Color.Gray.ToAndroid());
            UpdateBorders();
            //Control.SetTextSize(ComplexUnitType.Mm,(Element as NeatEditor).TextSize);
        }
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == NeatEditor.IsBorderErrorVisibleProperty.PropertyName
                || e.PropertyName == NeatEditor.HasFrameProperty.PropertyName)
            {
                UpdateBorders();
            }
        }
        void UpdateBorders()
        {
            GradientDrawable shape = new GradientDrawable();
            shape.SetShape(ShapeType.Rectangle);
            shape.SetCornerRadius(0);

            if ((Element as NeatEditor).HasFrame)
            {
                if ((Element as NeatEditor).IsBorderErrorVisible)
                {
                    shape.SetStroke(3, (Element as NeatEditor).BorderErrorColor.ToAndroid());
                }
                else
                {
                    shape.SetStroke(3, Android.Graphics.Color.LightGray);
                    this.Control.SetBackground(shape);
                }
            }
            else
            {
                shape.SetStroke(3, Color.Transparent.ToAndroid());
            }

            this.Control.SetBackground(shape);
        }

    }
}
