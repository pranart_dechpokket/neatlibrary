﻿using System;
using NeatLibrary;
using NeatLibrary.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(SafeView), typeof(SafeViewRenderer))]
namespace NeatLibrary.Droid
{
    public class SafeViewRenderer : ViewRenderer<SafeView,Android.Views.View>
    {
        public static void Init() { }

        public SafeViewRenderer()
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<SafeView> e)
        {
            base.OnElementChanged(e);

            Android.Views.View nativeView = new Android.Views.View(Xamarin.Forms.Forms.Context);

            var SafeView = Element;
            SafeView.NativeView = nativeView;

            SetNativeControl(nativeView);
        }
    }
}
