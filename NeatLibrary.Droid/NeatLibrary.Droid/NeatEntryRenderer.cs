using Xamarin.Forms;
using NeatLibrary;
using Xamarin.Forms.Platform.Android;

using NeatLibrary.Droid;
using Android.Util;
using Android.Graphics.Drawables;

[assembly: ExportRenderer (typeof (NeatEntry), typeof (NeatEntryRenderer))]
namespace NeatLibrary.Droid
{
	public class NeatEntryRenderer : EntryRenderer
	{
		public NeatEntryRenderer ()
		{
		}
		static public void Initialize() { }
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Entry> e)

		{
			base.OnElementChanged(e);

			Recreate ();
		}
		void Recreate()
		{

			if (this.Control == null) return;
			Control.SetPadding (0,0,0,0);

            Control.SetHintTextColor (Color.Gray.ToAndroid ());
            Control.SetTextSize (ComplexUnitType.Mm,(float)(Element as NeatEntry).TextSize);
            UpdateBorders();

		}
		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == "TextSize") 
			{
				Recreate ();
                UpdateBorders();
				this.Invalidate ();
			}
            else if(e.PropertyName == NeatEntry.IsBorderErrorVisibleProperty.PropertyName
                    || e.PropertyName == NeatEntry.HasFrameProperty.PropertyName)
            {
                UpdateBorders();
            }

		}
        void UpdateBorders()
        {
            GradientDrawable shape = new GradientDrawable();
            shape.SetShape(ShapeType.Rectangle);
            shape.SetCornerRadius(0);

            if ((Element as NeatEntry).HasFrame)
            {
                if (((NeatEntry)this.Element).IsBorderErrorVisible)
                {
                    shape.SetStroke(3, ((NeatEntry)this.Element).BorderErrorColor.ToAndroid());
                }
                else
                {
                    shape.SetStroke(3, Android.Graphics.Color.LightGray);
                    this.Control.SetBackground(shape);
                }
            }
            else
            {
                shape.SetStroke(3, Color.Transparent.ToAndroid());
            }

            this.Control.SetBackground(shape);
        }

	}
}

