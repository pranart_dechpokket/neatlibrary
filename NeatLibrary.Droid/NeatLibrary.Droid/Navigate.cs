﻿using System;
using Android.Content;
using NeatLibrary.Droid;
using Xamarin.Forms;

[assembly:Dependency(typeof(Navigate))]
namespace NeatLibrary.Droid
{
    public class Navigate : INavigate
    {
        public Navigate()
        {
        }

        public void NavigateTo(double latitude, double longitude, double fromLatitude, double fromLongitude)
        {
            string format = "geo:0,0?q=" + latitude + "," + longitude + "( Location title)";

            Android.Net.Uri uri = Android.Net.Uri.Parse(format);

            Intent intent = new Intent(Intent.ActionView, uri);
            intent.SetFlags(ActivityFlags.ClearTop);
            Forms.Context.StartActivity(intent);

        }
    }
}
