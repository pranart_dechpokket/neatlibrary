﻿using System;
using Xamarin.Forms;
using NeatLibrary;
using NeatLibrary.Droid;

[assembly:Dependency(typeof(VersionOs))]
namespace NeatLibrary.Droid
{
    public class VersionOs : IVersionOs
    {
        public VersionOs()
        {
        }

        public int VersionNumber()
        {
            return (int)Android.OS.Build.VERSION.SdkInt;
        }

        public string VersionString()
        {
            return Android.OS.Build.VERSION.Release;
        }
    }
}
