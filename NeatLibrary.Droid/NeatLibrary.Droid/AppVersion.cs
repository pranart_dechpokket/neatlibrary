﻿using System;
using NeatLibrary.Droid;
using Xamarin.Forms;

[assembly:Dependency(typeof(AppVersion))]
namespace NeatLibrary.Droid
{
    public class AppVersion : IAppVersion
    {
        public AppVersion()
        {
        }

        public string Version()
        {
            return Forms.Context.PackageManager.GetPackageInfo(Forms.Context.PackageName, 0).VersionName;
        }

    }
}
